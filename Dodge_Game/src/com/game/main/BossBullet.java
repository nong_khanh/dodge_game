package com.game.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class BossBullet extends GameObject{
	
	private Handler handler;
	Random r = new Random();

	public BossBullet(int x, int y, ID id, Handler handler)	 {
		super(x, y, id);
		this.handler = handler;
		velX = (r.nextInt(5 - -5) + -5);
		velY = 5;
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, 24, 24);
	}

	public void tick() {
		x += velX;
		y += velY;

		if(y >= Game.HEIGHT) handler.removeObject(this);
		handler.addObject(new Trail(x, y, ID.Trail, Color.pink, 24, 24, 0.05f, handler));
	}

	public void render(Graphics g) {

		g.setColor(Color.red);
		g.fillRect((int)x, (int)y, 24, 24);
	}

}
