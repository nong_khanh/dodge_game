package com.game.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

import com.game.main.Game.STATE;

public class Player extends GameObject{
	Random r = new Random();
	Handler handler;
	Color color;

	public Player(int x, int y, ID id, Color color, Handler handler) {
		super(x, y, id);
		this.color = color;
		this.handler = handler;	
	}	
	
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, 48, 48);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		x = Game.clamp(x, 0, Game.WIDTH - 64);
		y = Game.clamp(y, 0, Game.HEIGHT - 86);

		collision();
	}
	
	public void collision() {
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			if(tempObject.getId() == ID.BasicEnemy || tempObject.getId() == ID.FastEnemy || tempObject.getId() == ID.SmartEnemy) {
				if(getBounds().intersects(tempObject.getBounds())) {
					HUD.HEALTH-=2;
				}
			}
			if(tempObject.getId() == ID.HardEnemy) {
				if(getBounds().intersects(tempObject.getBounds())) {
					HUD.HEALTH-=4;
				}
			}
			if(tempObject.getId() == ID.EnemyBoss) {
				if(getBounds().intersects(tempObject.getBounds())) {
					HUD.HEALTH-=10;
				}
			}
		}
	}

	public void render(Graphics g) { 	
			g.setColor(color);
			g.fillRect((int)x, (int)y, 48, 48);
	}
}
