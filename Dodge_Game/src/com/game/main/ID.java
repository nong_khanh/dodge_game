package com.game.main;

public enum ID {
	Player(),
	Player2(),
	Trail(),
	FastEnemy(),
	SmartEnemy(),
	HardEnemy(),
	EnemyBoss(),
	MenuPartical(),
	BasicEnemy();
}
