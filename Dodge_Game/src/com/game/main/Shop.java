package com.game.main;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Shop extends MouseAdapter {
    Handler handler;
    HUD hud;
    private int B1= 1000;
    private int B2= 1000;
    private int B3= 1000;
    public Shop(Handler handler, HUD hud){
        this.handler= handler;
        this.hud = hud;
    }

    public void render(Graphics g){
        Font f1 = new Font("Mynerve", Font.PLAIN, 60);
        Color f2= new Color(236, 133, 185);
        g.setFont(f1);
        g.setColor(f2);
        g.drawString("SHOP", Game.WIDTH/2-100, 125);

        Font f3 = new Font("arial", 1, 24);
        g.setFont(f3);
        //box1
        g.drawString("Update Health", 120,275);
        g.drawString("Cost: "+B1, 120, 305);
        g.drawRect(100,220,200,120);

        //box2
        g.drawString("Update Speed", 390,275);
        g.drawString("Cost: "+B2, 390, 305);
        g.drawRect(380,220,200,120);

        //box 3
        g.drawString("Refill Health", 680,275);
        g.drawString("Cost: "+B3, 680, 305);
        g.drawRect(660,220,200,120);


        g.drawString("SCORE: " + hud.getScore(), 390,540);
        g.drawString("Press Space to go back", 340,600);
    }

    public void mousePressed(MouseEvent e){
        int mx= e.getX();
        int my= e.getY();

        //box 1
        if(mx>=100 && mx<=300){
            if(my>=220 && my<=340){
                if(hud.getScore()>=B1){
                    hud.setScore(hud.getScore()-B1);
                    B1+=500;
                    hud.bounds+=20;
                    hud.HEALTH += 20;
                }
            }
        }

        //box2 select
        if(mx>=380 && mx<=580){
            if(my>=220 && my<=340){
                if(hud.getScore()>=B2){
                    hud.setScore(hud.getScore()-B2);
                    B2+=500;
                    handler.spd++;
                }
            }
        }

        //box3 select
        if(mx>=660 && mx<=860){
            if(my>=220 && my<=340){
                if(hud.getScore()>=B3){
                    hud.setScore(hud.getScore()-B3);
                    B3+=500;
                    hud.HEALTH = (100+ (hud.bounds/2));
                }
            }
        }
    }
}