package com.game.main;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

public class Handler {
	LinkedList<GameObject> object = new LinkedList<GameObject>(); 
	public int spd = 5;
	
	public void tick() {
		for(int i = 0; i < object.size(); i++) {
			GameObject tempObject = object.get(i);
			tempObject.tick();
		}
	}
	
	public void render(Graphics g) {
		for(int i = 0; i < object.size(); i++) {
			GameObject tempObject = object.get(i);
			
			tempObject.render(g);
		}
	}
	
	public void clearEnemies() {  
		for(int i = 0; i < object.size(); i++) {
			GameObject tempObject = object.get(i);
			if(tempObject.getId() != ID.Player) {
				object.clear();
				if(Game.gameState == Game.STATE.Game)
				addObject(new Player((int)tempObject.getX(), (int)tempObject.getY(), ID.Player, Color.white, this));
				else if(Game.gameState == Game.STATE.TwoPlayers) {
					addObject(new Player((int)tempObject.getX(), (int)tempObject.getY(), ID.Player, Color.white, this));
					addObject(new Player((int)tempObject.getX(), (int)tempObject.getY(), ID.Player2, Color.orange, this));
				}
			}
		}
	}
	
	public void addObject(GameObject object) {
		this.object.add(object);
	}
	
	public void removeObject(GameObject object) {
		this.object.remove(object);
	}	
	
}
